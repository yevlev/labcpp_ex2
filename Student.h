/**
 * Student.h 
 *
 * --------------------------------------------------------------------------------------
 * General: This class represents a general student, holding his id number,
 *          and desired salary.
 *
 * Methods:  Student()    - Constructor. Gets student's id number and desired salary.
 *			 ~Student()   - Destructor.
 *
 *
 * --------------------------------------------------------------------------------------
 */


#ifndef STUDENT_H
#define STUDENT_H

#include <string>

using namespace std;


class Student {

public: 

	Student(const string& idNumber, double salary);
	virtual ~Student();

	const string& getIdNumber() const;
	double getSalary() const;

	void setSalary(double salary);

private:

	const string _idNumber;
	double _salary;

	//to forbid copy (even no need to implement them).
	Student(const Student& other);
	const Student& operator=(const Student& other);

};

#endif
