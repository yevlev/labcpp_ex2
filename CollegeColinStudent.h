/**
 * CollegeColinStudent.h 
 *
 * --------------------------------------------------------------------------------------
 * General: This class represents a 
 *
 * Methods:  CollegeColinStudent()    - Constructor. 
 *			 ~CollegeColinStudent()   - Destructor.
 *
 *
 * --------------------------------------------------------------------------------------
 */


#ifndef COLLEGE_COLIN_STUDENT_H
#define COLLEGE_COLIN_STUDENT_H

#include <string>

#include "CollegeStudent.h"

using namespace std;

class CollegeColinStudent : public CollegeStudent
{

public:

	CollegeColinStudent(double avgGrade);

	virtual ~CollegeColinStudent();

	double getAvgGrade() const;

	static double getCollegeAvg();

private:

	const double _avgGrade;
	const double _projGrade;

	static int numOfStudents;
	static double sumOfAvgGrades;
	
};

#endif
