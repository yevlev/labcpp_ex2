/**
 * UniversityStudent.h 
 *
 * --------------------------------------------------------------------------------------
 * General: This class represents a 
 *
 * Methods:  UniversityStudent()    - Constructor.
 *			 ~UniversityStudent()   - Destructor.
 *
 *
 * --------------------------------------------------------------------------------------
 */

#ifndef UNIVERSITY_STUDENT_H
#define UNIVERSITY_STUDENT_H

#include <string>

#include "Student.h"

using namespace std;


class UniversityStudent : public Student
{

public:

	UniversityStudent(const string& univName);
	virtual ~UniversityStudent();

	const string& getUnivName() const;


private:
	
	const string _univName;

	//to forbid copy (even no need to implement them).
	UniversityStudent(const UniversityStudent& other);
	const UniversityStudent& operator=(const UniversityStudent& other);

};

#endif
