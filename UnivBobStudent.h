/**
 * UnivBobStudent.h 
 *
 * --------------------------------------------------------------------------------------
 * General: This class represents a 
 *
 * Methods:  UnivBobStudent()    - Constructor. 
 *			 ~UnivBobStudent()   - Destructor.
 *
 *
 * --------------------------------------------------------------------------------------
 */


#ifndef UNIV_BOB_STUDENT_H
#define UNIV_BOB_STUDENT_H

#include <string>

#include "UniversityStudent.h"

using namespace std;

class UnivBobStudent : public UniversityStudent
{

public:

	UnivBobStudent(double avgGrade);

	virtual ~UnivBobStudent();

	double getAvgGrade() const;

	static double getUnivAvg();

private:

	const double _avgGrade;

	static int numOfStudents;
	static double sumOfAvgGrades;
	
};

#endif
