/**
 * UnivAliceStudent.h 
 *
 * --------------------------------------------------------------------------------------
 * General: This class represents a 
 *
 * Methods:  UnivAliceStudent()    - Constructor.
 *			 ~UnivAliceStudent()   - Destructor.
 *
 *
 * --------------------------------------------------------------------------------------
 */


#ifndef UNIV_ALICE_STUDENT_H
#define UNIV_ALICE_STUDENT_H

#include <string>

#include "UniversityStudent.h"

using namespace std;

class UnivAliceStudent : public UniversityStudent
{

public:

	UnivAliceStudent(double avgGrade);

	virtual ~UnivAliceStudent();

	double getAvgGrade() const;

	static double getUnivAvg();

private:

	const double _avgGrade;

	static int numOfStudents;
	static double sumOfAvgGrades;
	
};

#endif
