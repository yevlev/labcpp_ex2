/**
 * CollegeDannyStudent.h 
 *
 * --------------------------------------------------------------------------------------
 * General: This class represents a 
 *
 * Methods:  CollegeDannyStudent()    - Constructor.
 *			 ~CollegeDannyStudent()   - Destructor.
 *
 *
 * --------------------------------------------------------------------------------------
 */


#ifndef COLLEGE_DANNY_STUDENT_H
#define COLLEGE_DANNY_STUDENT_H

#include <string>

#include "CollegeStudent.h"

using namespace std;

class CollegeDannyStudent : public CollegeStudent
{

public:

	CollegeDannyStudent(double avgGrade);

	virtual ~CollegeDannyStudent();

	double getAvgGrade() const;

	static double getCollegeAvg();

private:

	const double _avgGrade;
	const double _projGrade;

	static int numOfStudents;
	static double sumOfAvgGrades;
	
};

#endif
